package com.cybage.response;

import java.util.List;

import com.cybage.model.Advertiser;
import com.cybage.model.MediaPlan;

public class Data {
	private Advertiser adv;
	private List<String> ad;
	private MediaPlan mp;
	private int mdplan;
	public int getMdplan() {
		return mdplan;
	}
	public void setMdplan(int mdplan2) {
		this.mdplan = mdplan2;
	}
	//private MediaPlan mediaPlan;
	private boolean Success;
	public List<String> getAd() {
		return ad;
	}
	public void setAd(List<String> adv) {
		this.ad = adv;
	}
	public boolean isSuccess() {
		return Success;
	}
	public void setSuccess(boolean success) {
		Success = success;
	}
	public Advertiser getAdv() {
		return adv;
	}
	public void setAdv(Advertiser adv2) {
		this.adv = adv2;
	}
	public MediaPlan getMp() {
		return mp;
	}
	public void setMp(MediaPlan mp) {
		this.mp = mp;
	}
	
	
	
	
}
