package com.cybage.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude
public class Response {
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public Response(Data data) {
		super();
		this.data = data;
	}
	
	
}
