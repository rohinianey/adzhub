package com.cybage.repository;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.cybage.model.Advertiser;
import com.cybage.model.MediaPlan;

@Repository
public class MediaRepository {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public MediaPlan getMediaPlan(int mid) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM media_plan WHERE mid = ? ", new Object[] { mid },
					new MediaPlan());
		} catch (EmptyResultDataAccessException e) {
			return new MediaPlan();
		}
	}

	public int addBudget(int mid, BigDecimal budget) {
		BigDecimal remainingBudget;
		try {
			remainingBudget = jdbcTemplate.queryForObject(
					"SELECT ( total_budget - sum(budget)) FROM advertiser ad JOIN media_plan mp ON ad.aid = mp.aid"
							+ " GROUP BY ad.aid" + " HAVING ad.aid = (SELECT aid FROM media_plan WHERE mid = ? )",
					new Object[] { mid }, BigDecimal.class);
		} catch (EmptyResultDataAccessException e) {
			return -1;
		}
		if (remainingBudget != null && budget.compareTo(remainingBudget) <= 0) {
			return jdbcTemplate.update("UPDATE media_plan SET budget = ? WHERE mid = ?", new Object[] { budget, mid });
		} else {
			return -1;
		}
	}


	public MediaPlan addMediaPlan(MediaPlan mp) {
		try {
			String sql = "INSERT INTO media_plan(aid,media_name,budget) VALUES(?,?,?)";
			KeyHolder holder = new GeneratedKeyHolder();
			jdbcTemplate.update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(sql, new String[] { "aid" });
					ps.setInt(1, mp.getAid());
					ps.setString(2, mp.getMediaName());
					ps.setBigDecimal(3, mp.getBudget());
					return ps;
				}
			}, holder);

			mp.setAid(holder.getKey().intValue());

		} catch (NullPointerException e) {
			System.out.println("err");
		}
		return mp;
	}
}
