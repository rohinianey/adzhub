package com.cybage.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.cybage.model.Advertiser;

@Repository
public class AdvertiserRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public List<String> getAd(Advertiser ad) {
		return jdbcTemplate.queryForList("select name from advertiser where aid='" + ad.getAid() + "'", String.class);
	}

	public Advertiser addAdvertiser(Advertiser ad) throws NullPointerException {
		try {
			String sql = "INSERT INTO advertiser(name,total_budget) VALUES(?,?)";
			KeyHolder holder = new GeneratedKeyHolder();
			jdbcTemplate.update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(sql, new String[] { "aid" });
					ps.setString(1, ad.getName());
					ps.setInt(2, ad.getTotalBudget());
					return ps;
				}
			}, holder);

			ad.setAid(holder.getKey().intValue());

		} catch (NullPointerException e) {
			System.out.println("err");
		}
		return ad;
	}

	public List<String> getAll(Advertiser ad) {
		return jdbcTemplate.queryForList(
				"select media_name from advertiser ad join media_plan mp on ad.aid=mp.aid where ad.aid='"
						+ ad.getAid() + "';",
				String.class);

	}
}
