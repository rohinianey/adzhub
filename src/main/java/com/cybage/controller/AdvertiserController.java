package com.cybage.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybage.model.Advertiser;
import com.cybage.repository.AdvertiserRepository;
import com.cybage.response.Data;
import com.cybage.response.Response;


@RestController
@RequestMapping(path="/adv")
public class AdvertiserController{
	@Autowired
	AdvertiserRepository adRepository;

	@PostMapping(path = "/add")
	public Response add(@RequestBody Advertiser ad){
		Advertiser adv =  adRepository.addAdvertiser(ad);
		Data data = new Data();
		data.setAdv(adv);
		data.setSuccess(true);
		return new Response(data);
	}
	@GetMapping(path="/get")
	public Response getAd(Advertiser ad){
		List<String> adv =  adRepository.getAd(ad);
		Data data = new Data();
		data.setAd(adv);
		data.setSuccess(true);
		return new Response(data);
	}

	
	@GetMapping(path="/getall")
	public Response getAll(Advertiser ad){
		List<String> adv =  adRepository.getAll(ad);
		Data data = new Data();
		data.setAd(adv);
		data.setSuccess(true);
		return new Response(data);
	
	}
	
	

}
