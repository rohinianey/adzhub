package com.cybage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybage.model.Advertiser;
import com.cybage.model.MediaPlan;
import com.cybage.repository.MediaRepository;
import com.cybage.response.Data;
import com.cybage.response.Response;

@RestController
@RequestMapping(path = "/media")
public class MediaController {
	@Autowired
	MediaRepository mediaRepository;

	MediaPlan mp;

	@PostMapping(path = "/add")
	public Response addMediaPlan(@RequestBody MediaPlan mp){
		MediaPlan media =  mediaRepository.addMediaPlan(mp);
		Data data = new Data();
		data.setMp(media);
		data.setSuccess(true);
		return new Response(data);
	}

	@PostMapping(path = "/addbudget")
	public Response addBudget(@RequestBody MediaPlan mp){
		
		int mdplan=mediaRepository.addBudget(mp.getMid(), mp.getBudget());
		Data data = new Data();
		data.setMdplan(mdplan);;
		data.setSuccess(true);
		return new Response(data);
	}
	
	
	@GetMapping(path = "/get")
	public Response getMediaPlan(int mid) {
		MediaPlan mediaPlan =  mediaRepository.getMediaPlan(mp.getMid());
		Data data = new Data();
		data.setMp(mediaPlan);
		data.setSuccess(true);
		return new Response(data);
	}

}
