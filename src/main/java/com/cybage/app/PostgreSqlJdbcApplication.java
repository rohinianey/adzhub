package com.cybage.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(value={"com.cybage.controller"})
@ComponentScan(value={"com.cybage.repository"})
@ComponentScan(value={"com.cybage.model"})
@SpringBootApplication
public class PostgreSqlJdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostgreSqlJdbcApplication.class, args);
	}

}

