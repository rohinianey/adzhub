package com.cybage.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.jdbc.core.RowMapper;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * The persistent class for the media_plan database table.
 * 
 */
@Data
public class MediaPlan implements Serializable, RowMapper<MediaPlan> {
	private static final long serialVersionUID = 1L;

	private int mid;
	
	@JsonProperty(required = true)
	private BigDecimal budget;

	private String mediaName;


	private Advertiser advertizer;

	private int aid;

	@Override
	public MediaPlan mapRow(ResultSet rs, int value) throws SQLException {
		MediaPlan mp = new MediaPlan();
		mp.setMid(rs.getInt(1));
		mp.setMediaName(rs.getString(2));
		mp.setBudget(rs.getBigDecimal(3));
		mp.setAid(rs.getInt(4));
		return mp;
	}

}
