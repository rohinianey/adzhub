package com.cybage.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * The persistent class for the advertiser database table.
 * 
 */
@Data
public class Advertiser {
	
	private int aid;

	private String name;

	private int totalBudget;



}